var artController = require('../controllers/artController');

module.exports = function (app) {
    app.get('/GetArtByDepartment/:id/:freeText', function (req, res, next) {
        artController.GetArtByDepartment(req.params.id,req.params.freeText).then(respone => res.send(JSON.stringify(respone)));
    })

};