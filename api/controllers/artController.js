var axios = require('axios');
var rateLimit = require('axios-rate-limit');
var { response: colorValues } = require("express");
var { getColorFromURL, getPaletteFromURL } = require('color-thief-node');

// The values are hard coded because we must be nice to Met Museum and we won't want it higher
var https = rateLimit(axios.create(), { maxRequests: 80, perMilliseconds: 1000 });

// In case we would need more properites we can add them to the dictionary.
const propertyList = {
    "objectID": "objectID",
    "primaryImage": "imageUrl"
};

// Change the value here to modify the amount of art works.
const maxImages = 50;

// I added the ability to change the department value for future requirements.
const searchArtByDepartmentURL = 'https://collectionapi.metmuseum.org/public/collection/v1/search?departmentId={0}&q={1}';

const getArtByID = 'https://collectionapi.metmuseum.org/public/collection/v1/objects/';
const imagePropertyName = "primaryImage";
const unexpectedErrorMessage = "An unexpected error occurred Here I would have sent a message to the logger";
const errorReturnMessage = "An error occurred while processing the information please try again later or with different variables";
const errorMessage = "The error: ";
const noPhotosMessage = "No photos in European department.";
const multiplePrimatryColors = "There are multiple dominant primary colors";

module.exports = {
    GetArtByDepartment: async function (departmentId, freeText) {
        try {
            // The Met API wouldn't respond without sending free text so I added it.
            let searchUrl = searchArtByDepartmentURL.replace("{0}", departmentId);
            searchUrl = searchUrl.replace("{1}", freeText);

            let response = await https.get(searchUrl);
            let ImagesInfo = await getImagesInfo(response.data);

            return ImagesInfo;

        } catch (error) {
            console.log(unexpectedErrorMessage);
            console.log(errorMessage + error);

            return errorReturnMessage;
        }

    }
};

async function getImagesInfo(imageList) {
    let processedImageList = [];

    if (imageList.total > 0) {
        let imageIds = imageList.objectIDs
        for (let counter = 0; counter < maxImages && counter < imageList.total; counter++) {
            processedImageList.push(GetImageByID(imageIds[counter]));
        }

        return await Promise.all(processedImageList);
    } else {
        return noPhotosMessage
    }
}

async function GetImageByID(imageID) {
    let image;

    try {
        let imageData = (await https.get(getArtByID + imageID)).data;
        image = await ExtractInfoFromImage(imageData);
    } catch (error) {
        console.log(unexpectedErrorMessage);
        console.log(errorMessage + error);

        return null;
    }

    return image;
}

async function ExtractInfoFromImage(imageData) {
    let image = {};

    for (key in propertyList) {
        image[propertyList[key]] = imageData[key];
    }

    // In case there is no image I do not write what the dominant colors are.
    if (imageData[imagePropertyName]) {
        colorValues = await getColorFromURL(imageData[imagePropertyName]);
        image.dominantColour = ConvertColorToHexValue(colorValues);

        colorValues = await getPaletteFromURL(imageData[imagePropertyName]);
        image.dominantPrimaryColor = getDominantPrimaryColor(colorValues);
    }

    return image;
}

function ConvertColorToHexValue(colorValues) {
    let hexValue = "#";

    for (let counter = 0; counter < colorValues.length; counter++) {
        let hex = colorValues[counter].toString(16);
        if (hex.length < 2) {
            hex = "0" + hex;
        }
        hexValue += hex;
    }
    return hexValue;
}

function getDominantPrimaryColor(colorValuesArray) {
    let colorValue = [0, 0, 0];
    for (let color in colorValuesArray) {
        colorValue[0] += colorValuesArray[color][0];
        colorValue[1] += colorValuesArray[color][1];
        colorValue[2] += colorValuesArray[color][2];
    }

    if (colorValues[0] == colorValues[1] && colorValues[2] == colorValues[1]) {
        return "None";
    } else {
        if (colorValues[0] < colorValues[1] && colorValues[2] < colorValues[1]) {
            return "Green";
        } else if (colorValues[0] < colorValues[2] && colorValues[1] < colorValues[2]) {
            return "Blue";
        } else if (colorValues[1] < colorValues[0] && colorValues[2] < colorValues[0]) {
            return "red";
        } else {
            return multiplePrimatryColors;
        }
    }
}